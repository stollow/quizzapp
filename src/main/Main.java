package main;

import models.Questions;
import models.Responses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args)throws IOException {
        int number_of_try = 0;
        boolean win = false;
        Responses response1 = new Responses("Paris");
        Questions question1 = new Questions(response1,"Quel est la capitale de la France ?");
        Responses response2 = new Responses("Berlin");
        Questions question2 = new Questions(response2,"Quel est la capitale de l'allemagne ?");
        Responses response3 = new Responses("Londre");
        Questions question3 = new Questions(response3,"Quel est la capitale de l'anglettere ?");
        List<Questions> QuestionsList = new ArrayList<>();
        QuestionsList.add(question1);
        QuestionsList.add(question2);
        QuestionsList.add(question3);
        Iterator<Questions> iterator = QuestionsList.iterator();
        while(iterator.hasNext()) {
            Questions InUseQuestion = iterator.next();
            InUseQuestion.askQuestions();
            win = false;
            while (!win) {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(System.in));
                String response = reader.readLine();
                if (InUseQuestion.verifyAnswer(response)) {
                    System.out.println("Bonne réponse");
                    System.out.println(number_of_try);
                    win = true;
                } else {
                    number_of_try++;
                    System.out.println("Mauvaise réponse");
                    System.out.println(number_of_try);
                }
            }
        }
    }
}
