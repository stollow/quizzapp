package models;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
public class QuestionsTests {
    Responses response1 = new Responses("Paris");
    Questions question1 = new Questions(response1,"Quel est la capitale de la France ?");
    @Test
    void verifyAnswerTest(){
        Assertions.assertFalse(question1.verifyAnswer("Berlin"));
        Assertions.assertTrue(question1.verifyAnswer("Paris"));
    }
}
