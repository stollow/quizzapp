package models;

public class Questions {

    private Responses response;
    private String question;

    public Questions(Responses response, String question) {
        this.response = response;
        this.question = question;
    }

    public Responses getResponse() {
        return response;
    }

    public void setResponse(Responses response) {
        this.response = response;
    }

    public void askQuestions(){
        System.out.println(this.question);
    }

    public boolean verifyAnswer(String player_response){
        if(player_response.equals(this.response.getResponse())){
            return true;
        }else{
            return false;
        }
    }
}
